<?php
//require_once './KinobazaApiClient.php';
//$filmName = 'Из ада';
//$filmName = 'From hell';
//$jsonfilmName = json_encode($filmName);
//dm($jsonfilmName);
//$file= file_get_contents("http://api.kinobaza.tv/films/search?fields_mask=1&query='$jsonfilmName'");
//
//dm(json_decode($file));
//die();
class Film {
  
//  var $JScode = 'var cat = new String();';
  var $picture = '';
  var $JScode = 'var cat = new String();';
  var $lang = '';
  function __construct() {
//    $this->JScode = 'var cat = new String();';
//    $this->lang = '';
//    $this->picture = '';
  }
  
  function getFilms() {
    global $db;
    $result = $db->query("SELECT * FROM films ORDER BY datestamp DESC");
    return $result;
  }
  
  function getFilm($filmId) {
    global $db;
    $result = $db->querySingle("SELECT * FROM films WHERE id = $filmId", 1);
    return $result;
  }
  
  function deleteFilm($filmId) {
    global $db;
    $result = $db->exec("DELETE FROM films WHERE id = $filmId");
    return $result;
  }
  
  function getConfig($param, $check = 0) {
    global $db;
    $result = $db->querySingle("SELECT * FROM tmdb WHERE parameter = '$param'", 1);
    if ($result['value'] === '' && $check === 1) return 'empty';
    return $result['value'];
  }
  
  function setConfig($param, $value) {
    global $db;
//    $value = addslashes($value);
    if (!$this->getConfig($param)) $result = $db->exec("INSERT INTO tmdb (parameter, value) VALUES ('$param', '$value')");
    return $result;
  }
  
  function updateConfig($param, $value) {
    global $db;
//    $value = addslashes($value);
//    3a968129526bcc01b81af4569506c18c
    $result = $db->exec("UPDATE tmdb SET value='$value' WHERE parameter ='$param'");
    return $result;
  }
  
  function addJS($param, $value) {
    $this->JScode .= "cat.$param = '$value';";
  }
}

class Themoviedb extends Film {

  function __construct() {
    $this->configCache = '';
    $this->APIkey = '';
  }

  function getLanguage() {
    
    $param = 'language';
//    $result = $this->getCache($param)['value'];
    $result = $this->getCache($param);
    
    if ($result) {
      
//      $doc = phpQuery::newDocumentHTML($result);
      global $lang;
      if ($lang) {
        $result = preg_replace('/selected/', '', $result);
        $result = preg_replace('/value="'.$lang.'"/', 'value="'.$lang.'" selected', $result);
      }
      
    } elseif (!$result) {
      
      include_once 'lib/phpQuery/phpQuery.php';
      $data = file_get_contents('http://www.themoviedb.org/movie/19995-avatar');
      phpQuery::newDocument($data);
      $list = pq('#language')->addClass('form-control')->htmlOuter();
      $result = $this->setCache($param, $list);
      if ($result) return $list;
      
    }
    return $result;
    
  }
  
//  function setLanguage() {
//    global $db;
////    $count = $db->query("SELECT COUNT(*) FROM tmdb WHERE name = language");
//    $count = $this->getConfig($param);
//    if ($count == 0) {
//      $list = $this->getLanguage();
//      $result = $db->exec("INSERT INTO films (parameter, value) VALUES ('language', '$list')");
//      return $result;
//    }
//  }
  /**
   * 
   * @global SQLite3 $db
   * @param string $param Parameter name
   * @param string $filmid Film ID
   * @param string $file_path_local Internal path of image
   * @return string
   */
  function getCache($param, $filmid = '', $file_path_local = '') {
    
    global $db;
    if ($filmid) {
      $query = "SELECT * FROM cache WHERE film_id = '$filmid'";
    } elseif ($file_path_local) {
      $query = "SELECT * FROM cache WHERE file_path_local = '$file_path_local'";
    } else {
      $query = "SELECT * FROM cache WHERE parameter = '$param'";
    }
    $result = $db->querySingle($query, 1);
    return $result;
  }
  
  /**
   * 
   * @global SQLite3 $db
   * @param type string $parameter Parameter name
   * @param type string $value Value of variable
   * @param type string $film_id  Film ID
   * @param type string $file_path_local Internal path of image
   * @param type string $overview Text film info
   * @return type
   */
  function setCache($parameter, $value, $film_id ='', $file_path_local ='', $overview ='') {
    
    global $db;
    
    $value = isset($value) ? htmlentities($value, ENT_QUOTES) : NULL;
    if ($parameter && !$film_id) {
      $query = "INSERT INTO cache ('parameter', 'value') VALUES ('$parameter', '$value')";
    } elseif (!$parameter && $film_id) {
      $overview = htmlentities($overview, ENT_QUOTES);
      if ($value) {
        $query = "INSERT INTO cache ('value', 'film_id', 'file_path_local', 'overview') VALUES ('$value', '$film_id', '$file_path_local', '$overview')";
      } else {
        $query = "INSERT INTO cache ('film_id', 'file_path_local', 'overview') VALUES ('$film_id', '$file_path_local', '$overview')";
      }
    }
    
//    $query = "INSERT INTO cache ('parameter', 'value', 'film_id', 'file_path_local', 'overview')";
//    $query .= !empty($parameter) ? "'parameter'" : NULL;
//    $query .= !empty($value) ? ", $value" : NULL;
//    $query .= !empty($film_id) ? ", $film_id" : NULL;
//    $query .= !empty($file_path_local) ? ", $file_path_local" : NULL;
//    $query .= !empty($overview) ? ", $overview" : NULL;
//    
//    $query .= "VALUES ('$parameter', '$value'";
//    
//    $query .= !empty($parameter) ? ", $parameter" : NULL;
//    $query .= !empty($value) ? ", $value" : NULL;
//    $query .= !empty($film_id) ? ", $film_id" : NULL;
//    $query .= !empty($file_path_local) ? ", $file_path_local" : NULL;
//    $query .= !empty($overview) ? ", $overview" : NULL;
//    $query .= ')';
//    dm($query);
    $result = $db->exec($query);
    return $result;
  }

  function clearChache() {
    $query = 'DELETE * FROM cache';
    $result = $db->querySingle($query, 1);   
    return $result;
  }
          
  function getRemoteFileSize($url){ 
     $parse = parse_url($url); 
     $host = $parse['host']; 
     $fp = @fsockopen ($host, 80, $errno, $errstr, 20); 
     if(!$fp){  
       $ret = 0;  
     }else{ 
       $host = $parse['host']; 
       fputs($fp, "HEAD ".$url." HTTP/1.1\r\n"); 
       fputs($fp, "HOST: ".$host."\r\n");  
       fputs($fp, "Connection: close\r\n\r\n"); 
       $headers = ""; 
       while (!feof($fp)){  
         $headers .= fgets ($fp, 128);  
       } 
       fclose ($fp); 
       $headers = strtolower($headers); 
       $array = preg_split("|[\s,]+|",$headers); 
       $key = array_search('content-length:',$array); 
       $ret = $array[$key+1]; 
     }   
     if($array[1]==200) return $ret; 
     else return -1*$array[1]; 
   } 

  function save_image($remoteImg, $place){
    $curl = curl_init($remoteImg);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_BINARYTRANSFER,1);
    $rawdata = curl_exec($curl);
    curl_close($curl);
//    if (file_exists($place)) :
//     unlink($place);
//    endif;
    $fp = fopen($place,'x');
    $result = fwrite($fp, $rawdata);
    fclose($fp);
    return $result;
  }
  
  function getFilmInfo($filmId) {

  }
  
}

$film = new Film();
$tmdb = new Themoviedb();
$db = new SQLite3('films.db');
//$res = $tmdb->getCache('', '');
//dm($res);
$film->lang = $film->getConfig('language');
$tmdb->APIkey = $tmdb->getConfig('configApiKey');
if ($film->getConfig('configCache') === 'on') {
  $tmdb->configCache = 'checked';
  $Cache = 1;
}
$film->addJS('language', $film->lang);
$film->addJS('apikey', $tmdb->APIkey);


//$db->query('CREATE TABLE films (bar varchar(10))');
//$db->query("INSERT INTO foo VALUES ('fnord')");
//$result = $db->query('select bar from foo');
//$result = $db->query($query);
//while ($row = $result->fetchArray()) {
//  dm($row);
//}

if ($_POST['deleteId']) {
  
  $result = $film->deleteFilm($_POST['deleteId']);
  if (!$result) echo '0';
  die();
  
} elseif ($_POST['action'] === 'edit') {
  
  $engName = isset($_POST['engname']) ? 'nameeng = "'.$_POST['engname'].'"' : NULL;
  $year = isset($_POST['year']) ? 'year = "'.$_POST['year'].'"' : NULL;
  $filmid = isset($_POST['filmid']) ? $_POST['filmid'] : NULL;
  if ($engName && $year) $engName .= ',';
  $query = "UPDATE films SET $engName $year WHERE id = $filmid";
  $result = isset($filmid) ? $db->exec($query) : NULL;
  if (!$result) print '0';
  die();  

} elseif ($_POST['action'] === 'saveConfig') {
  
  parse_str($_POST['data'], $data);
  if ($film->getConfig('configCache') && $_POST['configCache'] === 'false')  {
    $film->updateConfig('configCache', $_POST['configCache']);
  }
  foreach ($data as $param => $value) {
    $paramCheck = $film->getConfig($param,1);
    if ($paramCheck || $paramCheck === 'empty') {
      $result = $film->updateConfig($param, $value);
    } elseif (!$paramCheck && $value) {
      $result = $film->setConfig($param, $value);
    }
  }
  print $result;
  die();
  
} elseif ($_POST['action'] === 'clearCache') {
  

  print $result;
  die();
  
} elseif ($_POST['action'] === 'getFilmInfo') {
  
  $filmId = $_POST['filmId'];
//  $filmData = $film->getFilm($filmId);
  $filmDataCache = $tmdb->getCache('', $filmId);
//  dm($filmDataCache);
// dm($filmDataCache = is_null($filmDataCache) ? 'yess' : 'no');
  if (count($filmDataCache) == 0) {
    
    $filmNameLocal = $film->getFilm($filmId)['name'];
    $urlImage = 'http://api.themoviedb.org/3/search/movie?api_key=' . $tmdb->APIkey . '&query=' . urlencode($filmNameLocal);
    $filmDataImage = json_decode(file_get_contents($urlImage));
//    dm($filmDataImage);
    $urlOverview = 'http://api.themoviedb.org/3/movie/' . $filmDataImage->results[0]->id . '?api_key=' . $tmdb->APIkey . '&language=' . $film->lang;
    $filmDataOverview = json_decode(file_get_contents($urlOverview));
//    $filmDataOverview = json_decode(file_get_contents($urlOverview));
//    print('non-cached');
//    print file_get_contents('http://image.tmdb.org/t/p/w185'. '/umXaJpx6MMO9MKx2KIcMzPctVyF.jpg');
    
    $currentMonth = date('Y.m');
    $dir = is_dir("cache/$currentMonth");
    if (!$dir) {
      $dir = mkdir("cache/$currentMonth", 0775);
    }
    
    $fileRemote = 'http://image.tmdb.org/t/p/w185' . $filmDataImage->results[0]->poster_path;
//    $fileRemoteHandle = fopen($fileRemote, "rb");
//    $size = $tmdb->getRemoteFileSize($fileRemote); 
    
//    if($size==0) echo "Не могу соединиться"; 
//    elseif($size<0) echo "Ошибка. Код ответа HTTP: ".(-1*$size); 
//    else echo "Размер удалённого файла (bytes): ".$size;         
    
//    $contents = fread($fileRemoteHandle, $size);
    $fileLocal = "cache/$currentMonth" .$filmDataImage->results[0]->poster_path;
//    $fileLocalHandle = fopen($fileLocal, "w+b");
    if (!is_file($fileLocal)){
//      $film->picture = $fileLocal;
//      $data['file'] = $fileLocal;
//    } else {
//      $ch = curl_init($fileRemote);
      
      $result = $tmdb->save_image($fileRemote, "cache/$currentMonth/".basename($fileRemote));
      
    } else {
      $result = true;
    }
      
      
//        copy ($fileRemote, "cache/$currentMonth/".basename($fileRemote));
  //      $result = fwrite($fileLocalHandle, $contents);
  //      fclose($fileLocalHandle);
//      dm($result);
      if ($result) {
  //      dm($filmDataImage->results[0]->poster_path);
  //      dm(iconv('CP1251', 'UTF-8', $filmDataOverbiew->overview));
        $tmdb->setCache('', implode('*',array($filmDataOverview->original_title, substr($filmDataOverview->release_date, 0, 4))), $filmId, 'cache/'.$currentMonth . $filmDataImage->results[0]->poster_path , $filmDataOverview->overview);
      }
    
    $data = Array(
        'file' => $fileLocal,
        'overview' => html_entity_decode($filmDataOverview->overview),
        'engName' => html_entity_decode($filmDataOverview->original_title),
        'year' => substr($filmDataOverview->release_date, 0, 4)
    );    
//    fclose($fileRemoteHandle);
    
    
  } else {
//    print('cached');
    $value = explode('*', $filmDataCache['value']);
    $data = Array(
        'file' => $filmDataCache['file_path_local'],
        'overview' => html_entity_decode($filmDataCache['overview']),
        'engName' => html_entity_decode($value[0]),
        'year' => $value[1]
    );
  }
  print json_encode($data);
//  print_r($data);
  die();
  
} elseif ($_POST) {
  
  $name = $_POST['name'];
  $nameeng = $_POST['nameeng'];
  $year = $_POST['year'];
  $rating = $_POST['rating'];
  $datestamp = time();
  $result = $db->exec("INSERT INTO films (name, nameeng, year, rating, datestamp) VALUES ('$name', '$nameeng', '$year', '$rating', '$datestamp')");
  if (!$result) echo '0';
  die();
  
}

$result = $film->getFilms();
?>
<!doctype html>
<html>
  <meta charset="utf-8" />
<head>
  <link rel="shortcut icon" href="/images/favicon.png" type="image/png">
</head>
<div class="table-responsive tableContainer" id='filmList'>
  <div class="buttonSet">
    <div><button id='config' class="btn btn-default btn-sm pull-left"><span class="glyphicon glyphicon-wrench"></span> Configure</button></div>
    <div><button id='search' class="btn btn-default btn-sm pull-left"><span class="glyphicon glyphicon-search"></span> Search</button></div>
    <div class="pull-left"><input type="text" id='searchField' class="form-control input-sm hidden" placeholder="Search me"></div>
    <div><button id='addFilm' class='btn btn-default btn-sm'>Add a film</button></div>
    <div><h3 class='text-center'>Films list:</h3></div>
  </div>
  <table id='main' class='table table-striped table-bordered table-hover table-condensed tablesorter'>
  <!--<table id='main' class='tablesorter'>-->
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>English Name</th>
        <th>Year</th>
        <th>Rating</th>
        <th>Added</th>
        <th>Delete</th>
      </tr>
    </thead>
<?php ?>
    <?php
    while ($row = $result->fetchArray()) {
//  print_r($row['id']);
      ?>
      <tr <?php print 'data-id=' . $row['id'] ?> >
        <td><?php print $row['id'] ?></td>
        <td><?php print "<div><a href='#'>" . $row['name'] . "</a></div>" ?></td>
        <td><?php print $row['nameeng'] ?></td>
        <td><?php print $row['year'] ?></td>
        <td class='star'><?php print $row['rating'] ?></td>
        <td><?php if ($row['datestamp']) print date("m.d.Y", $row['datestamp']) ?></td>
        <td><p class="center glyphicon glyphicon-floppy-remove"></p></td>
      </tr>


<?php } ?>
  </table>
</div>
<?php


/**
 * helper
 * @param any type $var
 */
function dm($var, $plaintext = 0) {
  print '<pre>';
  if ($plaintext) print '<plaintext>';
  var_dump($var);
  if ($plaintext) print '</plaintext>';
  print '</pre>';
}

include_once './template.php';
?>


