$(document).ready(function() {

  var rating$, filmId$, filmQueryresult$;
  var loader$ = $('#loader');
  var saveButton$ = $('#save');
  var addFilm$ = $('#addFilmForm');
  var configFilm$ = $('#configFilmForm');

  var infoFilm$ = $('#infoFilm');
  var confirmDelete$ = $("#confirmDelete");
  var fBContentLiSet$ = $('#fBContent li');
  var fixBlock$ = $('#fixBlock');
  var configStatus$ = $('#configStatus');
  var fixFilmInfo$ = $('#fBfix');
  var clearCache$ = $('#clearCache');
  var search$ = $('#search');
  var searchField$ = $('#searchField');
  var mainTable$ = $('#main')
  
  $.ajaxSetup({
    type: 'POST',
    url: '/',
    dataType: 'json',
    beforeSend: function(xhr) {
//            loader$.removeClass('hide');
//            loader$.removeClass('invisible');
      loader$.show();
      saveButton$.attr('disabled', 'disabled');
    },
    complete: function() {
//            loader$.addClass('hide');
//            loader$.addClass('invisible');
      loader$.hide();
      saveButton$.removeAttr('disabled');
    }
  });

  $(".rating").rateBar({
    defaultStarColor: '#777777',
    ratedStarColor: '#FFD700',
    onRate: function(rate) {
      //      console.log(rate);
      rating$ = rate;
    },
    setRate: function(rate) {
      ;
    }
  });

  $('.star').ratingbar({
    maxRating: 5,
    wrapperWidth: 80,
    showText: false,
    wrapperClass: "star_empty",
    innerClass: "star_filled"
  });

  saveButton$.click(function(e) {

    e.preventDefault();

    var name$ = $('#inputName').val();
    var nameEng$ = $('#inputNameEng').val();
    var year$ = $('#inputYear').val();
    $.ajax({
      data: {
        name: name$,
        nameeng: nameEng$,
        year: year$,
        rating: rating$
      },
      beforeSend: function() {
        if (name$.length < 3) {
          $('#inputName').parent().addClass('has-error');
          return false;
        }
      },
      complete: function () {
        location.reload();
        
      }
    });


  });

  // dialog add new film 
  addFilm$.dialog({
    autoOpen: false,
    height: 290,
    width: 350,
    position: {
      my: "top",
      at: "bottom",
      of: '#filmList'
    },
    open: function(event, ui) {
      //      addFilm$.parent().hide();
    }
  });
  addFilm$.parent().zIndex(1500);

  // dialog configuration
  configFilm$.dialog({
    autoOpen: false,
//        height: 290,
    width: 500,
    buttons: {
      "Save": function() {
//                $(this).dialog("close");
        var $string = $('form[name="configdata"]').serialize();
        $.ajax({
          data: {
            action: 'saveConfig',
            data: $string,
            configCache: $('input[name="configCache"]').prop("checked")
          }
        }).done(function() {
          location.reload();
//                  configStatus$.show();
//                  configStatus$.text('OK').hide({effect: 'highlight', duration : 3000, color: '#ffffff'});
        });
      },
      Cancel: function() {
        $(this).dialog("close");
      }
    }
  });
  configFilm$.parent().zIndex(1300);

  // dialog film info
  infoFilm$.dialog({
    autoOpen: false,
    width: 800,
    //    zIndex: 99,
    //    open: function(event, ui) {
    //      infoFilm$.parent().hide();
    //    },
    close: function(event, ui) {
      //      infoFilm$.parent().hide();
      fBContentLiSet$.each(function() {
        $(this).addClass('hidden').text(' ');
        fixBlock$.addClass('hidden');
      });
    }
  });

  confirmDelete$.dialog({
    autoOpen: false,
    resizable: false,
    height: 155,
    width: 330,
    modal: true,
    title: 'Confirmation',
    //    open: function(event, ui) {
    //      $(this).parent().hide();
    //    },
    buttons: {
      Delete: function() {
        $(this).dialog("close");
        $.ajax({
          data: {
            'deleteId': filmId$
          },
          complete: function () {
            location.reload();

          }          
        });
      },
      Cancel: function() {
        $(this).dialog("close");
      }
    }
  });

  $('#addFilm').click(function() {
    //    addFilm$.parent().show();
    addFilm$.dialog('open');
  });

  $('#config').click(function() {
    //    configFilm$.parent().show();
    configFilm$.dialog('open');
  });


  /**
   * Title click link handler
   */
  $('#filmList a').click(function() {

    var filmId$ = $(this).parents('tr').attr('data-id');
    var text$ = $(this).text();
    var $picture, parentTr$, rusName$, engName$, year$;
    var infoFilmPicture$ = $('#infoFilmPicture > img');
    var infoFilmOverview$ = $('#infoFilmOverview');

    if (cat.apikey.length !== 32) {
      infoFilmOverview$.text('Set API key in configuration menu');
      infoFilm$.dialog('open');
      infoFilm$.dialog({
        title: 'Error'
      });
      return 0;
    }

    $.ajax({
      data: {
        action: 'getFilmInfo',
        filmId: filmId$
      }
//      ,
//      complete: function (data) {
//      }      
    }).done(function (data) {
      
        filmQueryresult$ = data;
  //            cl(filmQueryresult$.file);
  //            cl(filmQueryresult$.engName);
  //            cl(filmQueryresult$.year);
  //            cl('/---------------/');
  //            cl(filmQueryresult$);
  //            $picture = filmQueryresult$.results[0].poster_path;
        infoFilmPicture$.attr('src', filmQueryresult$.file);
        infoFilmOverview$.text(filmQueryresult$.overview);
        filmQueryresult$.filmid = filmId$;
        infoFilm$.dialog('open');
        infoFilm$.dialog({
          title: text$
        });
    });

//        $.ajax({
//            type: 'GET',
//            url: 'http://api.themoviedb.org/3/search/movie?api_key=' + cat.apikey + '&query=' + text$,
//            async: false
//        }).done(function(data) {
//            filmQueryresult$ = data;
//            $picture = filmQueryresult$.results[0].poster_path;
//            $pictureSrc = 'http://image.tmdb.org/t/p/w185/' + $picture;
//            infoFilmPicture$.attr('src', $pictureSrc);
//        });
//
//        $.ajax({
//            type: 'GET',
//            url: 'http://api.themoviedb.org/3/movie/' + filmQueryresult$.results[0].id + '?api_key=' + cat.apikey + '&language=' + cat.language,
//            async: false
//        }).done(function(data) {
//            infoFilmOverview$.text(data.overview);
//            infoFilm$.dialog('open');
//            infoFilm$.dialog({
//                title: text$
//            });
//        });

    // Check all fields
    parentTr$ = $(this).parents('tr');
    rusName$ = parentTr$.find('td').eq(1).find('a').text();
    engName$ = parentTr$.find('td').eq(2).text();
    year$ = parentTr$.find('td').eq(3).text();

//        cl(filmQueryresult$.results[0].original_title);
//        cl(filmQueryresult$.results[0].release_date);
    if (!rusName$) {
      fixBlock$.removeClass('hidden');
      fBContentLiSet$.eq(0).removeClass('hidden').text('Add Russian name');
    }
    ;
    if (!engName$) {
      fixBlock$.removeClass('hidden');
      fBContentLiSet$.eq(1).removeClass('hidden').text('Add English name');
//            filmQueryresult$.engName = filmQueryresult$.results[0].original_title;
    }
    ;
    if (!year$) {
      fixBlock$.removeClass('hidden');
      fBContentLiSet$.eq(2).removeClass('hidden').text('Add Year');
      filmQueryresult$.year = (/\d{4}/.exec(filmQueryresult$.year));
    }
    ;

  });

  /**
   * Fix information about film button handler
   */
  fixFilmInfo$.click(function() {
    if (typeof filmQueryresult$.engName !== 'undefined' || typeof filmQueryresult$.year !== 'undefined') {
//      cl(filmQueryresult$.filmid);
//      cl(filmQueryresult$.engName);
//      cl(filmQueryresult$.year);
      $.ajax({
        data: {
          action: 'edit',
          filmid: filmQueryresult$.filmid,
          engname: filmQueryresult$.engName,
          year: filmQueryresult$.year
        },
        complete: function () {
          location.reload();

        }        
      });
    }

  });

  /**
   * Delete film button handler
   */
  $('table td:last-child p').click(function() {
    confirmDelete$.dialog('open');
    filmId$ = $(this).parents('tr').attr('data-id');
  });


  $('#configWhere').popover({
    placement: 'left',
    trigger: 'hover',
    title: 'Where to get?',
    html: true,
    delay: {hide: '2000'},
    content: 'You need to register there <a href="http://www.themoviedb.org" target="_blank">www.themoviedb.org</a></br> Than you need to get the key in API section of your account config'
  });
  
  clearCache$.click(function () {
    $.ajax({
      data: {
        action: 'clearCache'
      },
      complete: function () {
        location.reload();

      }        
    });    
  });


  search$.click(function () {
    searchField$.toggleClass('hidden');
    searchField$.val('');
    searchField$.focus();
    searchField$.livefilter.clear({selector:'#main tbody tr'});
    
  });
  
  searchField$.livefilter({selector:'#main tbody tr'});
  mainTable$.tablesorter();
  
  /**
   * Add hotkeys
   */

  //  document.addEventListener('keypress', function(e) {
  //
  //    var key = e.keyCode;
  //    if (key === 96) { // v
  //      $(".ui-dialog-content").dialog("close");
  //    } else if (key === 102) { //f
  ////            window.location.assign('http://xcraft.ru/w#fleet');
  //    } else {
  //      ;
  //    }
  //  }, false);
});

/**
 * Helper
 * @param {var} text
 * @returns {print in console}
 */

function cl(text) {
  console.log(text);
}