
  <head>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">
    <meta content="/images/favicon.png" itemprop="image">
    <link rel="stylesheet"  type="text/css" href="styles/styles.css">
    <link rel="stylesheet"  type="text/css" href="styles/pnotify.custom.min.css">
    <link rel="stylesheet" href="//yandex.st/bootstrap/3.0.3/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="//yandex.st/jquery-ui/1.10.4/themes/start/jquery-ui.min.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf-8" src="//yandex.st/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="//yandex.st/bootstrap/3.0.3/js/bootstrap.min.js"></script>    
    <!--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>-->
    <script src="scripts/jquery.ratingbar.js"></script>
    <script src="scripts/rating-master/jquery.rating.js"></script>
    <!--<script src="scripts/jquery.oauth.js"></script>-->
    <script type="text/javascript" charset="utf-8" src="//yandex.st/jquery-ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="//yandex.st/jquery-ui/1.10.4/jquery.ui.dialog.min.js"></script>
    <script type="text/javascript" charset="utf-8"><?php print $film->JScode ?></script>
    <script src="scripts/main.js"></script>
    <script src="scripts/jquery.livefilter.min.js"></script>
    <script src="scripts/jquery.tablesorter.min.js"></script>
    <script src="scripts/pnotify.custom.min.js"></script>


  </head>
  <body>
    <div id='loader'></div>
    <div class='container'>
      <div id="configForm" title="Configuration"></div>
      <div id="addFilmForm" title="Add a film" >
        <form name="filmdata" method="post" action='/' class="form-horizontal" role="form">
          <div class="form-group">
            <!--<label for="inputName" class="col-sm-2 control-label">Name:<div class="mark"> *</div></label>-->
            <div class="col-sm-10">
              <input type="text" name='name' class="form-control" id="inputName" placeholder="Name" required="required" >
            </div>
          </div>        
          <div class="form-group">
            <!--<label for="inputNameEng" class="col-sm-2 control-label">Eng. name:</label>-->
            <div class="col-sm-10">
              <input type="text" name='nameeng' class="form-control" id="inputNameEng" placeholder="English name" >
            </div>
          </div>
          <div class="form-group">
            <!--<label for="inputYear" class="col-sm-2 control-label">Year:</label>-->
            <div class="col-sm-10">
              <input type="text" name='year' class="form-control" id="inputYear" placeholder="Year">
            </div>
          </div>        
          <!--<input type="text" name="rating">-->
          <div class="form-group">
            <label for="inputRating" class="col-sm-2 control-label">Rating:</label>
            <div class="col-sm-10">
              <div class="rating"></div>
            </div>
          </div>         
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-10">
              <button type="submit" id="save" class="btn btn-default">Save</button>
            </div>
          </div>        

        </form>
      </div>
      <div id="infoFilm" title="Film information" >
        <div id="infoFilmPicture"><img></div>
        <div id="infoFilmOverview"></div>
        <div id="fixBlock" class="hidden">
          <div class="panel panel-warning">
            <div class="panel-heading">
              <h3 class="panel-title inline" id="fBTitle">Unfilled fields</h3>
              <button type="button" id="fBfix" class="btn btn-success inline">Fix it!</button>
            </div>
            <div class="panel-body" id="fBContent">
              You can
              <ul>
                <li class="hidden"></li>
                <li class="hidden"></li>
                <li class="hidden"></li>
              </ul>
            </div>
          </div>          
          <div></div>
          <div></div>
        </div>
      </div>
      <div id="configFilmForm" title="Configuration" >
        <div id="configInfo"></div>
        <form name="configdata" method="post" action='/' class="form-horizontal" role="form">
          <!--<input type="checkbox">-->
          <fieldset>
            <!--<form class="form-horizontal" role="form">-->
              <legend>
                Common
              </legend>
              <legend>
                Themoviedb.org
                <text id="configStatus"></text>
              </legend>
              <p>
                <input type="text" id="configApiKey" name="configApiKey" class="form-control" placeholder="API key" value="<?php print $tmdb->APIkey ?>">
                <text id="configWhere">Where to get?</text>
              </p>
              <p>
                <?php print $tmdb->getLanguage(); ?>
              </p>
              <p>
                <input type="checkbox" name="configCache" <?php print $tmdb->configCache ?>> Activate cache system
              </p>
              <p>
                <input type="button" id="clearCache" value="Clear cache">
              </p>
                
            <!--</form>-->
          </fieldset>
        </form>
      </div>
      <div id="confirmDelete">Are you sure want to delete this film?</div>
    </div>
  </body>
</html>